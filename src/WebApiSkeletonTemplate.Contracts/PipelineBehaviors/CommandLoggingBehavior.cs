﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WebApiSkeleton.Security.Core.UserIdentity;

namespace WebApiSkeletonTemplate.Contracts.PipelineBehaviors;

public sealed class CommandLoggingBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    where TRequest : notnull
{
    private readonly ILogger<CommandLoggingBehavior<TRequest, TResponse>> _logger;
    private readonly IUserIdentity _userIdentity;

    public CommandLoggingBehavior(ILogger<CommandLoggingBehavior<TRequest, TResponse>> logger, 
        IConfiguration configuration, 
        IUserIdentity userIdentity)
    {
        _logger = logger;
        _userIdentity = userIdentity;
    }

    public async Task<TResponse> Handle(TRequest request,
        RequestHandlerDelegate<TResponse> next,
        CancellationToken cancellationToken)
    {
        var currentRequestGuid = Guid.NewGuid();
        if (_userIdentity.IsAuthenticated)
        {
            _logger.LogInformation("[{Guid}] User with identifier {Id} called instance of {TRequest}",
                currentRequestGuid, _userIdentity.UserId, typeof(TRequest).Name);
        }
        else
        {
            _logger.LogInformation("[{Guid}] Anonymous user called instance of {TRequest}", currentRequestGuid,
                typeof(TRequest).Name);
        }

        try
        {
            var result = await next();
            _logger.LogInformation("[{Guid}] Successfully completed instance of {TRequest}", currentRequestGuid,
                typeof(TRequest).Name);
            return result;
        }
        catch (Exception)
        {
            _logger.LogError("[{Guid}] Error executing request", currentRequestGuid);
            throw;
        }
    }
}