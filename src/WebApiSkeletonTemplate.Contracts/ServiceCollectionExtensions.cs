﻿using System.Reflection;
using MediatR.NotificationPublishers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace WebApiSkeletonTemplate.Contracts;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddContractsAssemblyDependencies(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddDomainMediatR();
        return services;
    }

    private static IServiceCollection AddDomainMediatR(this IServiceCollection services)
    {
        services.AddMediatR(configuration =>
        {
            configuration.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly());
            configuration.NotificationPublisher = new TaskWhenAllPublisher();
        });
        return services;
    }
}