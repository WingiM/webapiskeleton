﻿using WebApiSkeletonTemplate.Domain.Models;

namespace WebApiSkeletonTemplate.Contracts.Responses;

public sealed record PongResponse(PingInformation Info);