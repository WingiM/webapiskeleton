﻿using WebApiSkeletonTemplate.Domain.Models;

namespace WebApiSkeletonTemplate.Contracts.DomainEvents;

public sealed record PingNotification(PingInformation Info) : INotification;