// Global using directives

global using LanguageExt.Common;
global using MediatR;
global using WebApiSkeleton.Contracts.Base;