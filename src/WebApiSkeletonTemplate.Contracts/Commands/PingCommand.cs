﻿using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeletonTemplate.Domain.Models;
using WebApiSkeleton.Security.Attributes;
using WebApiSkeletonTemplate.Contracts.Responses;

namespace WebApiSkeletonTemplate.Contracts.Commands;

[SecurityRequirements(RequiredRoles = ["Pinger"], RequiredPermissions = ["Ping"])]
public record PingCommand(PingInformation Info)
    : IAuthorizedRequest<PongResponse>, IValidatableRequest<PongResponse>, ITransactRequest;