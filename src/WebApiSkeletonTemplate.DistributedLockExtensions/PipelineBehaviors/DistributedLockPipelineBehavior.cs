﻿using System.Reflection;
using LanguageExt.Common;
using MediatR;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.DistributeLockUtilities.Exceptions;

namespace WebApiSkeletonTemplate.DistributedLockExtensions.PipelineBehaviors;

public sealed class DistributedLockPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    where TRequest : IDistributeLockableRequest
{
    public async Task<TResponse> Handle(TRequest request,
        RequestHandlerDelegate<TResponse> next,
        CancellationToken cancellationToken)
    {
        if (typeof(TResponse).GetGenericTypeDefinition() != typeof(Result<>).GetGenericTypeDefinition())
            return await next();

        try
        {
            return await next();
        }
        catch (DistributedLockException lockException)
        {
            var ctor = typeof(TResponse).GetConstructor(BindingFlags.Public | BindingFlags.Instance, new[] { typeof(Exception) });
            return (TResponse)ctor!.Invoke(new object?[] { lockException });
        }
    }
}