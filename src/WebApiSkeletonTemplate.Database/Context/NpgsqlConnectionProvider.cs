﻿using System.Data;
using Microsoft.EntityFrameworkCore.Storage;

namespace WebApiSkeletonTemplate.Database.Context;

public class NpgsqlConnectionProvider
{
    private readonly ApplicationContext _context;
    
    public NpgsqlConnectionProvider(ApplicationContext context)
    {
        _context = context;
    }

    public (IDbConnection connection, IDbTransaction? transaction) GetConnection()
    {
        var connection = _context.Database.GetDbConnection();
        return (connection, _context.Database.CurrentTransaction?.GetDbTransaction());
    }
}