﻿using WebApiSkeletonTemplate.Database.Models;

namespace WebApiSkeletonTemplate.Database.Context;

public class ApplicationContext : DbContext
{
    public ApplicationContext(DbContextOptions<ApplicationContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
#if Example
        modelBuilder.Entity<PingInformationDb>()
            .ToTable("ping", "public");
#endif
    }
}