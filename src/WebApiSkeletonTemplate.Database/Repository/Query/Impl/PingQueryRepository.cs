﻿using WebApiSkeleton.DatabaseMapping;
using WebApiSkeletonTemplate.Domain.Models;
using WebApiSkeleton.SearchSqlGeneration;
using WebApiSkeleton.SearchUtilities;
using WebApiSkeletonTemplate.Database.Context;
using WebApiSkeletonTemplate.Database.Models;
using WebApiSkeletonTemplate.Database.Repository.Base;

namespace WebApiSkeletonTemplate.Database.Repository.Query.Impl;

internal sealed class PingQueryRepository : QueryRepositoryBase, IPingQueryRepository
{
    private readonly IDatabaseMapping<PingInformationDb> _pingMapping;

    public PingQueryRepository(NpgsqlConnectionProvider connectionProvider, ISearchSqlConverter searchSqlConverter,
        IDatabaseMapping<PingInformationDb> pingMapping) :
        base(connectionProvider, searchSqlConverter)
    {
        _pingMapping = pingMapping;
    }

    public async Task<ListResult<PingInformation>> GetPingInformation(ListParam<PingInformation> param)
    {
        var propertyMapping = new Dictionary<string, string>
        {
            { nameof(PingInformation.RecordId), nameof(PingInformationDb.Id) },
            { nameof(PingInformation.Id), nameof(PingInformationDb.Info) }
        };

        var convertedFilters = ConvertFilters<PingInformation, PingInformationDb>(param.Filters, propertyMapping);
        FilterDefinition<PingInformationDb>[] additionalFilters =
        [
            new FilterDefinition<PingInformationDb>(x => x.IsActual, FilterOperand.Equals, true)
        ];

        var convertedSortingDefinitions =
            ConvertSortingDefinitions<PingInformation, PingInformationDb>(param.SortingDefinitions, propertyMapping);

        var parameters = new DynamicParameters();
        var selectStatement = SqlConverter.GetSelectStatement<PingInformationDb>([x => x.Info, x => x.Id], "p");
        var filtersSql = GetFiltersSql(convertedFilters.Concat(additionalFilters), parameters, "p");
        var orderBySql = GetOrderBySql(convertedSortingDefinitions, _pingMapping[nameof(PingInformationDb.Id)]!, "p");
        var paginationSql = SqlConverter.ToSql(param.PaginationInfo);

        var countSql = $"""
                        SELECT COUNT(*)
                        FROM {_pingMapping.SchemaQualifiedTableName} p
                        WHERE {filtersSql}
                        """;
        var resultSql = $"""
                         SELECT {selectStatement}
                         FROM {_pingMapping.SchemaQualifiedTableName} p
                         WHERE {filtersSql}
                         ORDER BY {orderBySql}
                         {paginationSql}
                         """;

        var (connection, transaction) = ConnectionProvider.GetConnection();
        var count = await connection.ExecuteScalarAsync<int>(countSql, parameters, transaction);
        var result = await connection.QueryAsync<PingInformationDb>(resultSql, parameters, transaction);

        return new ListResult<PingInformation>(result.Select(x => new PingInformation { RecordId = x.Id, Id = x.Info }),
            count);
    }
}