﻿using WebApiSkeletonTemplate.Domain.Models;
using WebApiSkeleton.SearchUtilities;

namespace WebApiSkeletonTemplate.Database.Repository.Query;

public interface IPingQueryRepository
{
    public Task<ListResult<PingInformation>> GetPingInformation(ListParam<PingInformation> param);
}