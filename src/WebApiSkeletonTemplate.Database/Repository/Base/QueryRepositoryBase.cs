﻿using WebApiSkeleton.SearchSqlGeneration;
using WebApiSkeleton.SearchUtilities;
using WebApiSkeletonTemplate.Database.Context;

namespace WebApiSkeletonTemplate.Database.Repository.Base;

public abstract class QueryRepositoryBase
{
    protected readonly NpgsqlConnectionProvider ConnectionProvider;
    protected readonly ISearchSqlConverter SqlConverter;

    protected QueryRepositoryBase(NpgsqlConnectionProvider connectionProvider, ISearchSqlConverter sqlConverter)
    {
        ConnectionProvider = connectionProvider;
        SqlConverter = sqlConverter;
    }

    protected IEnumerable<FilterDefinition<TTarget>> ConvertFilters<TSource, TTarget>(
        IEnumerable<FilterDefinition<TSource>> filters, Dictionary<string, string>? propertyMappings = null)
        where TSource : class
        where TTarget : class => filters.Select(x =>
        x.ToFilterDefinition<TSource, TTarget>(
            propertyMappings?.TryGetValue(x.PropertyName, out var targetPropertyName) ?? false
                ? targetPropertyName
                : null));

    protected IEnumerable<SortingDefinition<TTarget>> ConvertSortingDefinitions<TSource, TTarget>(
        IEnumerable<SortingDefinition<TSource>> sortingDefinitions,
        Dictionary<string, string>? propertyMappings = null)
        where TTarget : class
        where TSource : class => sortingDefinitions.Select(x =>
        x.ToSortingDefinition<TSource, TTarget>(
            propertyMappings?.TryGetValue(x.PropertyName, out var targetPropertyName) ?? false
                ? targetPropertyName
                : null));

    protected string GetFiltersSql<T>(IEnumerable<FilterDefinition<T>> filters, DynamicParameters parameters,
        string? tableAlias = null) where T : class
    {
        var whereConditions = new List<string>();
        foreach (var filter in filters)
        {
            var sqlFilter = SqlConverter.ToSqlFilter(filter, tableAlias);
            if (sqlFilter is null)
                continue;
            parameters.Add(sqlFilter.ParameterName, sqlFilter.Value);
            whereConditions.Add(sqlFilter.Sql);
        }

        return whereConditions.Count == 0 ? "" : $" {string.Join(" AND ", whereConditions)} ";
    }

    protected string GetOrderBySql<T>(IEnumerable<SortingDefinition<T>> sortingDefinitions, string defaultSortColumn,
        string? tableAlias = null) where T : class
    {
        var sortConditions = sortingDefinitions
            .Select(x => SqlConverter.ToSql(x, tableAlias))
            .ToList();

        return $" {(sortConditions.Count != 0 ? string.Join(", ", sortConditions) : defaultSortColumn)} ";
    }
}