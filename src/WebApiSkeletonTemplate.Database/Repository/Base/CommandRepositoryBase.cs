﻿using WebApiSkeletonTemplate.Database.Context;

namespace WebApiSkeletonTemplate.Database.Repository.Base;

public class CommandRepositoryBase
{
    protected readonly ApplicationContext Context;

    public CommandRepositoryBase(ApplicationContext context)
    {
        Context = context;
    }
}