﻿using WebApiSkeletonTemplate.Domain.Models;

namespace WebApiSkeletonTemplate.Database.Repository.Command;

public interface IPingCommandRepository
{
    public Task<PingInformation> SavePingInfo(PingInformation pingInformation);
}