﻿using WebApiSkeletonTemplate.Domain.Models;
using WebApiSkeleton.Security.Core.UserIdentity;
using WebApiSkeletonTemplate.Database.Context;
using WebApiSkeletonTemplate.Database.Models;
using WebApiSkeletonTemplate.Database.Repository.Base;

namespace WebApiSkeletonTemplate.Database.Repository.Command.Impl;

public sealed class PingCommandRepository : CommandRepositoryBase, IPingCommandRepository
{
    private readonly IUserIdentity _userIdentity;

    public PingCommandRepository(ApplicationContext context, IUserIdentity userIdentity) : base(context)
    {
        _userIdentity = userIdentity;
    }

    public async Task<PingInformation> SavePingInfo(PingInformation pingInformation)
    {
        var entity = new PingInformationDb
        {
            Info = pingInformation.Id,
            ReceivedAt = DateTime.UtcNow,
            ChangedBy = _userIdentity.UserId,
            ChangedOn = DateTime.UtcNow,
            IsActual = true
        };
        Context.Set<PingInformationDb>().Add(entity);
        await Context.SaveChangesAsync();
        return new PingInformation { Id = pingInformation.Id, RecordId = entity.Id };
    }
}