﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using WebApiSkeleton.DatabaseMapping;
using WebApiSkeleton.SearchSqlGeneration;
using WebApiSkeleton.SearchSqlGeneration.Postgres;
using WebApiSkeletonTemplate.Database.Context;
#if Example
using WebApiSkeletonTemplate.Database.Mapping;
using WebApiSkeletonTemplate.Database.Models;
using WebApiSkeletonTemplate.Database.Repository.Command;
using WebApiSkeletonTemplate.Database.Repository.Command.Impl;
using WebApiSkeletonTemplate.Database.Repository.Query;
using WebApiSkeletonTemplate.Database.Repository.Query.Impl;
#endif

namespace WebApiSkeletonTemplate.Database;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDatabaseAssemblyDependencies(this IServiceCollection services,
        IConfiguration configuration)
    {
        DefaultTypeMap.MatchNamesWithUnderscores = true;
        services.ConfigureDatabase(configuration);
        return services;
    }

    private static void ConfigureDatabase(this IServiceCollection services,
        IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("Default");

        if (connectionString is null)
            throw new Exception("No database connection string");

        services.AddDbContext<ApplicationContext>(options =>
        {
            options.UseNpgsql(connectionString,
                npgsqlOptions => npgsqlOptions.UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery));
            options.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
            options.UseSnakeCaseNamingConvention();
        });

        services.AddScoped<NpgsqlConnectionProvider>();
#if Example
        services.AddSingleton<IDatabaseMapping<PingInformationDb>, PingInformationDbMap>();
        services.AddScoped<IPingQueryRepository, PingQueryRepository>();
        services.AddScoped<IPingCommandRepository, PingCommandRepository>();
#endif
        services.AddScoped<ISearchSqlConverter, PostgresSearchSqlConverter>();
    }
}