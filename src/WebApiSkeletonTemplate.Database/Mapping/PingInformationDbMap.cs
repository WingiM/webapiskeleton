﻿using WebApiSkeleton.DatabaseMapping;
using WebApiSkeletonTemplate.Database.Models;

namespace WebApiSkeletonTemplate.Database.Mapping;

internal sealed class PingInformationDbMap : BaseDatabaseMapper<PingInformationDb>
{
    public PingInformationDbMap() : base("public.ping")
    {
        MapProperty(x => x.Id, "id", true);
        MapProperty(x => x.Info, "info");
        MapProperty(x => x.ChangedBy, "changed_by");
        MapProperty(x => x.ChangedOn, "changed_on");
        MapProperty(x => x.IsActual, "is_actual");
    }
}