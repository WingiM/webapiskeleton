﻿using WebApiSkeletonTemplate.Database.Models.Base;

namespace WebApiSkeletonTemplate.Database.Models;

public class PingInformationDb : TrackableEntity<int>
{
    public Guid Info { get; set; }
    public DateTime ReceivedAt { get; set; }
}