﻿namespace WebApiSkeletonTemplate.Database.Models.Base;

public class TrackableEntity<T> : DatabaseEntity<T>
{
    public bool IsActual { get; set; }
    public DateTime ChangedOn { get; set; }
    public int ChangedBy { get; set; }
}