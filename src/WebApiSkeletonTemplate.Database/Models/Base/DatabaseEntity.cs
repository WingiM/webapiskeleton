﻿namespace WebApiSkeletonTemplate.Database.Models.Base;

/// <summary>
/// Base database entity having a primary key
/// </summary>
/// <typeparam name="T">Primary key type</typeparam>
[PrimaryKey("Id")]
public abstract class DatabaseEntity<T>
{
    public T? Id { get; set; }
}