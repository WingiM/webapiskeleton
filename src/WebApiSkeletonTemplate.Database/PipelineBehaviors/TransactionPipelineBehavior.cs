﻿using MediatR;
using WebApiSkeleton.Contracts.Base.ContractFeatureInterfaces;
using WebApiSkeleton.Security.Contracts;
using WebApiSkeletonTemplate.Database.Context;

namespace WebApiSkeletonTemplate.Database.PipelineBehaviors;

public sealed class TransactionPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    where TRequest : ITransactRequest
{
    private readonly ApplicationContext _context;

    public TransactionPipelineBehavior(ApplicationContext context)
    {
        _context = context;
    }

    public async Task<TResponse> Handle(TRequest request,
        RequestHandlerDelegate<TResponse> next,
        CancellationToken cancellationToken)
    {
        if (request.GetType().Assembly == typeof(RegistrationCommand).Assembly)
            return await next();

        await using var transaction = await _context.Database.BeginTransactionAsync(cancellationToken);
        try
        {
            var result = await next();
            await transaction.CommitAsync(cancellationToken);
            return result;
        }
        catch (Exception)
        {
            await transaction.RollbackAsync(cancellationToken);
            throw;
        }
    }
}