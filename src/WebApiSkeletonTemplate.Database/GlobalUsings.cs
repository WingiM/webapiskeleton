// Global using directives

global using Dapper;
global using Microsoft.EntityFrameworkCore;
global using Npgsql;