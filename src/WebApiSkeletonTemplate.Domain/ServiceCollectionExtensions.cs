﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Serilog;


namespace WebApiSkeletonTemplate.Domain;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDomainAssemblyDependencies(this IServiceCollection services,
        IConfiguration configuration)
    {
        var serializerSettings = new JsonSerializerSettings
        {
            TypeNameHandling = TypeNameHandling.All
        };
        services.AddSingleton(serializerSettings);
        return services;
    }
    
    public static IHostBuilder ConfigureDomain(this IHostBuilder host)
    {
        host.ConfigureLogging();
        return host;
    }
    
    private static IHostBuilder ConfigureLogging(this IHostBuilder host)
    {
        host.UseSerilog((context, configuration) =>
        {
            configuration
                .ReadFrom
                .Configuration(context.Configuration)
                .WriteTo.Console()
                .WriteTo.File("logs/domain-logs.txt", rollingInterval: RollingInterval.Day);
        });
        return host;
    }
}