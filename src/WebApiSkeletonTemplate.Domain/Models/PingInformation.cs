﻿namespace WebApiSkeletonTemplate.Domain.Models;

public sealed class PingInformation
{
    public int RecordId { get; init; }
    public Guid Id { get; init; }
}