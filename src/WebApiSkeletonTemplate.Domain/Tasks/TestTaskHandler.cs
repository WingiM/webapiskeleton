﻿using Microsoft.Extensions.Logging;
using WebApiSkeleton.Tasks.TaskHandlers;

namespace WebApiSkeletonTemplate.Domain.Tasks;

public sealed class TestTaskHandler : ITaskHandler<TestTask>
{
    private readonly ILogger<TestTaskHandler> _logger;

    public TestTaskHandler(ILogger<TestTaskHandler> logger)
    {
        _logger = logger;
    }

    public Task Handle(TestTask task)
    {
        _logger.LogInformation("Received task {TaskId} with GUID {Guid}", task.Id, task.Guid);
        return Task.CompletedTask;
    }
}