﻿using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeletonTemplate.Domain.Tasks;

public sealed class TestTask : BaseBackgroundTask
{
    public Guid Guid { get; init; }
}