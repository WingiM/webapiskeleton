﻿using Microsoft.Extensions.Logging;
using WebApiSkeleton.Tasks.Integrity;
using WebApiSkeleton.Tasks.Models;
using WebApiSkeleton.Tasks.Tasks;

namespace WebApiSkeletonTemplate.Domain.Tasks;

public sealed class TaskIntegrityService : ITaskIntegrityService
{
    private readonly ILogger<TaskIntegrityService> _logger;

    public TaskIntegrityService(ILogger<TaskIntegrityService> logger)
    {
        _logger = logger;
    }

    public Task ChangeTaskStatusAsync(TaskState taskState)
    {
        _logger.LogInformation("Task status received: {Id} {State} {Percent} {Comment}", taskState.TaskId,
            (TaskStates)taskState.StateId, taskState.PercentComplete, taskState.Comment);
        return Task.CompletedTask;
    }

    public Task SaveTaskAsync(BaseBackgroundTask task)
    {
        _logger.LogInformation("Task received: {Id}", task.Id);
        return Task.CompletedTask;
    }
}