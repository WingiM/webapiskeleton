﻿using MediatR;
using Microsoft.Extensions.Logging;
using WebApiSkeletonTemplate.Contracts.DomainEvents;

namespace WebApiSkeletonTemplate.Ping.Handlers;

internal sealed class PingNotificationHandler : INotificationHandler<PingNotification>
{
    private readonly ILogger<PingNotificationHandler> _logger;

    public PingNotificationHandler(ILogger<PingNotificationHandler> logger)
    {
        _logger = logger;
    }

    public Task Handle(PingNotification notification, CancellationToken cancellationToken)
    {
        _logger.LogInformation("Received ping {Info}!", notification.Info);
        return Task.CompletedTask;
    }
}