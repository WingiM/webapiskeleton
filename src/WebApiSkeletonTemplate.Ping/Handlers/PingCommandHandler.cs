﻿using LanguageExt.Common;
using MediatR;
using Microsoft.Extensions.Logging;
using WebApiSkeletonTemplate.Contracts.Commands;
using WebApiSkeletonTemplate.Contracts.DomainEvents;
using WebApiSkeletonTemplate.Contracts.Responses;
using WebApiSkeletonTemplate.Database.Repository.Command;
using WebApiSkeletonTemplate.Database.Repository.Query;
using WebApiSkeletonTemplate.Domain.Models;
using WebApiSkeleton.SearchUtilities;

namespace WebApiSkeletonTemplate.Ping.Handlers;

internal sealed class PingCommandHandler : IRequestHandler<PingCommand, Result<PongResponse>>
{
    private readonly IPublisher _publisher;
    private readonly ILogger<PingCommandHandler> _logger;
    private readonly IPingCommandRepository _commandRepository;
    private readonly IPingQueryRepository _queryRepository;

    public PingCommandHandler(IPublisher publisher, ILogger<PingCommandHandler> logger,
        IPingCommandRepository commandRepository, IPingQueryRepository queryRepository)
    {
        _publisher = publisher;
        _logger = logger;
        _commandRepository = commandRepository;
        _queryRepository = queryRepository;
    }

    public async Task<Result<PongResponse>> Handle(PingCommand request, CancellationToken cancellationToken)
    {
        var res = await _commandRepository.SavePingInfo(request.Info);
        var foundPing = await _queryRepository.GetPingInformation(new ListParam<PingInformation>
        {
            Filters = [new FilterDefinition<PingInformation>(x => x.RecordId, FilterOperand.Equals, res.RecordId)]
        });
        await _publisher.Publish(new PingNotification(request.Info), cancellationToken);
        return new PongResponse(foundPing.Items.First());
    }
}