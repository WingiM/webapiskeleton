﻿using System.Reflection;
using Microsoft.Extensions.DependencyInjection;

namespace WebApiSkeletonTemplate.Ping;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddPingExample(this IServiceCollection services)
    {
        services.AddMediatR(x => x.RegisterServicesFromAssembly(Assembly.GetExecutingAssembly()));
        return services;
    }
}