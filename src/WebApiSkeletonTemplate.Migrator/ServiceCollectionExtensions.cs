﻿using System.Reflection;
using FluentMigrator.Runner;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace WebApiSkeletonTemplate.Migrator;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddMigratorAssemblyDependencies(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddFluentMigratorCore()
            .ConfigureRunner(r => r
                .AddPostgres()
                .WithGlobalConnectionString(configuration.GetConnectionString("Default")!)
                .ScanIn(Assembly.GetExecutingAssembly())
                .For.Migrations());

        services.AddHostedService<MigrationHostedService>();
        return services;
    }
}