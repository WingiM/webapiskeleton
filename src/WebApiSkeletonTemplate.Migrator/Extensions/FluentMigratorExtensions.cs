﻿using System.Reflection;
using FluentMigrator.Builders;
using FluentMigrator.Builders.Create.Table;
using FluentMigrator.Infrastructure;

namespace WebApiSkeletonTemplate.Migrator.Extensions;

public static class FluentMigratorExtensions
{
    public static ICreateTableWithColumnSyntax WithPrimaryKey(this ICreateTableWithColumnSyntax createTable,
        string columnName = "id", Type? columnType = null,
        PostgresGenerationType generationType = PostgresGenerationType.ByDefault)
    {
        var integerColumnType = columnType ?? typeof(int);
        return createTable
            .WithColumn(columnName)
            .WithIntegerType(integerColumnType)
            .PrimaryKey()
            .Identity(generationType);
    }

    public static ICreateTableColumnOptionOrForeignKeyCascadeOrWithColumnSyntax WithForeignColumn(
        this ICreateTableWithColumnSyntax columnBuilder,
        string primaryTableName,
        string primaryTableSchema = "public",
        string primaryColumnName = "id",
        string? foreignKeyName = null,
        string? foreignColumnName = null,
        Type? columnType = null)
    {
        var integerColumnType = columnType ?? typeof(int);
        foreignColumnName ??= primaryTableName + "_id";
        foreignKeyName ??= $"fk_{primaryTableName}_{foreignColumnName}";

        return columnBuilder
            .WithColumn(foreignColumnName)
            .WithIntegerType(integerColumnType)
            .ForeignKey(foreignKeyName, primaryTableSchema, primaryTableName, primaryColumnName);
    }

    public static ICreateTableWithColumnSyntax WithTrackingColumns(this ICreateTableWithColumnSyntax createTable,
        string? foreignKeyName = default)
    {
        foreignKeyName ??= "fk_user_changed_by";

        return createTable
            .WithColumn("is_actual").AsBoolean().WithDefaultValue(true)
            .WithColumn("changed_on").AsCustom("timestamp with time zone").WithDefault(SystemMethods.CurrentDateTime)
            .WithColumn( "changed_by").AsInt32();
    }

    private static TFluent WithIntegerType<TFluent>(this IColumnTypeSyntax<TFluent> columnTypeBuilder,
        MemberInfo integerType) where TFluent : IFluentSyntax
    {
        var typeName = integerType.Name;
        return typeName switch
        {
            nameof(Int16) => columnTypeBuilder.AsInt16(),
            nameof(Int32) => columnTypeBuilder.AsInt32(),
            nameof(Int64) => columnTypeBuilder.AsInt64(),
            _ => throw new NotSupportedException("Cannot create a column with passed integer type")
        };
    }
}