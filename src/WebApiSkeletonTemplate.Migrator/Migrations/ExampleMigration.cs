﻿using WebApiSkeletonTemplate.Migrator.Extensions;

namespace WebApiSkeletonTemplate.Migrator.Migrations;

[Migration(2024_04_21_01, "Example migration")]
public sealed class ExampleMigration : ForwardOnlyMigration
{
    public override void Up()
    {
        Create
            .Table("ping")
            .InSchema("public")
            .WithPrimaryKey("id", typeof(int))
            .WithColumn("info")
            .AsGuid()
            .NotNullable()
            .WithColumn("received_at")
            .AsDateTime()
            .NotNullable()
            .WithTrackingColumns();
    }
}