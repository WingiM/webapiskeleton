﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using WebApiSkeletonTemplate.IdentityMigrations;
using WebApiSkeleton.Security.Core.Data;

namespace WebApiSkeletonTemplate.API;

public sealed class DesignTimeIdentityContext : IDesignTimeDbContextFactory<IdentityContext>
{
    public IdentityContext CreateDbContext(string[] args)
    {
        var builder = new DbContextOptionsBuilder<IdentityContext>();

        builder.UseNpgsql("fake",
                opt => opt.MigrationsAssembly(typeof(IdentityMigrations.IdentityMigrations).Assembly.FullName))
            .UseSnakeCaseNamingConvention();
        return new IdentityContext(builder.Options);
    }
}