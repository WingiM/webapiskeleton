﻿using System.Net;
using WebApiSkeletonTemplate.Migrator;

namespace WebApiSkeletonTemplate.API.Middleware;

public class ExceptionMiddleware
{
    private readonly ILogger<ExceptionMiddleware> _logger;
    private readonly RequestDelegate _next;

    public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
    {
        _next = next;
        _logger = logger;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _next(context);
        }
        catch (MigrationException me)
        {
            _logger.LogCritical("Migrations failed with error: {Exception}", me.ToString());
            throw;
        }
        catch (Exception e)
        {
            _logger.LogError("Unhandled exception received: {Exception}", e.ToString());
            context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
            await context.Response.WriteAsJsonAsync(new
            {
                Message =
                    "Произошла внутренняя ошибка сервера. Если ошибка повторяется неоднократно, свяжитесь с разработчиком"
            });
        }
    }
}