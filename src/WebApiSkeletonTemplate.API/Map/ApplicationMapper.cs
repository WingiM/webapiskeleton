﻿using Riok.Mapperly.Abstractions;
#if Example
using WebApiSkeletonTemplate.Domain.Models;
using WebApiSkeletonTemplate.API.Input;
#endif

namespace WebApiSkeletonTemplate.API.Map;

[Mapper]
public partial class ApplicationMapper
{
#if Example
    public partial PingInformation Map(PingInput input);
#endif
}