﻿namespace WebApiSkeletonTemplate.API.Input;

public sealed class PingInput
{
    public Guid Id { get; set; }
}