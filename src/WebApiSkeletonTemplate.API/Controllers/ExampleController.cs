﻿#if Example
using MediatR;
using Microsoft.AspNetCore.Mvc;
using WebApiSkeletonTemplate.Contracts.Commands;
using WebApiSkeletonTemplate.Domain.Tasks;
using WebApiSkeleton.FileSystem.Abstractions.Models;
using WebApiSkeleton.FileSystem.Contracts;
using WebApiSkeleton.FileSystem.MinIO;
using WebApiSkeleton.FileSystem.MinIO.Services;
using WebApiSkeleton.Security.Contracts;
using WebApiSkeleton.Security.Core.Models.DTO;
using WebApiSkeleton.Tasks.Queue.Client;
using WebApiSkeletonTemplate.API.Input;
using WebApiSkeletonTemplate.API.Map;

namespace WebApiSkeletonTemplate.API.Controllers;

[ApiController]
[Route("/[controller]/[action]")]
public sealed class ExampleController : BaseControllerWithUtils
{
    private readonly ISender _sender;
    private readonly ApplicationMapper _mapper;
    private readonly IClientBackgroundTaskQueue _clientQueue;
    private readonly IMinioService _minioService;
    private readonly MinioSettings _minioSettings;

    public ExampleController(ISender sender, IClientBackgroundTaskQueue clientQueue, IMinioService minioService, MinioSettings minioSettings)
    {
        _sender = sender;
        _clientQueue = clientQueue;
        _minioService = minioService;
        _minioSettings = minioSettings;
        _mapper = new ApplicationMapper();
    }

    [HttpGet]
    public async Task<IActionResult> Ping([FromQuery] PingInput pingInput)
    {
        var info = _mapper.Map(pingInput);
        var command = new PingCommand(info);
        var res = await _sender.Send(command);
        return MatchResult(res);
    }

    [HttpPost]
    public async Task<IActionResult> EnqueueTestTask()
    {
        var task = new TestTask { Guid = Guid.NewGuid() };
        await _clientQueue.EnqueueTask(task);
        return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> UploadFile(IFormFile file)
    {
        await _minioService.CreateBucket(_minioSettings.DefaultBucketName);
        await using var uploadStream = file.OpenReadStream();
        var uploadFileCommand =
            new UploadFileCommand(new UploadingFile(new FileInformation(file.FileName), file.ContentType, uploadStream));
        var res = await _sender.Send(uploadFileCommand);
        return MatchResult(res);
    }

    [HttpGet]
    public async Task<IActionResult> DownloadFile(string fileName)
    {
        var downloadFileCommand = new DownloadFileCommand(new FileInformation(fileName));
        var res = await _sender.Send(downloadFileCommand);
        return MatchResult(res, x => File(x.DownloadStream, x.FileInfo.ContentType));
    }

    [HttpPost]
    public async Task<IActionResult> GetAuthorizationToken()
    {
        var command = new AuthorizationCommand("SkeletonAdmin", "TestWebApiSkeletonAdminPassword123!",
            Request.Headers.UserAgent!);
        var res = await _sender.Send(command);
        if (res.IsFaulted)
        {
            var registerCommand = new RegistrationCommand(new SecurityRegistrationCredentials
            {
                Email = "webapiskeletonadmin@gmail.com",
                IsEnabled = true,
                Password = "TestWebApiSkeletonAdminPassword123!",
                Username = "SkeletonAdmin",
                RoleNames = ["Pinger"]
            }, Request.Headers.UserAgent!);
            var registerResult = await _sender.Send(registerCommand);
            return MatchResult(registerResult);
        }

        return MatchResult(res);
    }
}
#endif