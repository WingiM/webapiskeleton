﻿using System.Text;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using StackExchange.Redis;
using WebApiSkeletonTemplate.Contracts;
using WebApiSkeletonTemplate.Contracts.PipelineBehaviors;
using WebApiSkeleton.Contracts.Validation;
#if Example
using WebApiSkeletonTemplate.ContractValidation.Messages;
#endif
using WebApiSkeletonTemplate.Database;
using WebApiSkeletonTemplate.Database.PipelineBehaviors;
using WebApiSkeleton.DistributeLockUtilities.Settings;
using WebApiSkeletonTemplate.Domain;
using WebApiSkeletonTemplate.Domain.Tasks;
using WebApiSkeleton.FileSystem.Contracts;
using WebApiSkeleton.FileSystem.MinIO;
using WebApiSkeletonTemplate.Migrator;
#if Example
using WebApiSkeletonTemplate.Ping;
#endif
using WebApiSkeleton.Security.Core;
using WebApiSkeleton.Security.Core.Extensions.DatabaseContextExtensions;
using WebApiSkeleton.Security;
using WebApiSkeleton.Security.Contracts;
using WebApiSkeleton.Security.Core.Settings;
using WebApiSkeleton.Tasks;
using WebApiSkeleton.Tasks.Settings;
using WebApiSkeletonTemplate.DistributedLockExtensions.PipelineBehaviors;

namespace WebApiSkeletonTemplate.API;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddReferencedAssembliesDependencies(this IServiceCollection services,
        IConfiguration configuration)
    {
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            IssuerSigningKey =
                new SymmetricSecurityKey(Encoding.ASCII.GetBytes(configuration["JwtSettings:SigningKey"]!)),
            ValidateIssuer = false,
            ValidateAudience = false,
            ValidateLifetime = false,
            RequireExpirationTime = false,
            ClockSkew = TimeSpan.FromDays(1)
        };
        services
            .AddContractsAssemblyDependencies(configuration)
            .AddDomainAssemblyDependencies(configuration)
            .AddDatabaseAssemblyDependencies(configuration)
            .AddMigratorAssemblyDependencies(configuration)
            .AddSecurityCore(config =>
            {
                config.DatabaseOptions = new IdentityDatabaseOptions
                {
                    OptionsAction = options => options
                        .UseNpgsql(configuration.GetConnectionString("Security"),
                            npgsqlOptions => npgsqlOptions
                                .UseQuerySplittingBehavior(QuerySplittingBehavior.SplitQuery)
                                .MigrationsHistoryTable("identity_migrations",
                                    "identity")
#if IdentityMigrations
                                .MigrationsAssembly(typeof(IdentityMigrations.IdentityMigrations).Assembly.FullName)
#endif
                        )
                        .UseSnakeCaseNamingConvention()
                        .UseDefaultSchema("identity"),
                    UseRequestTransactions = true
                };
                config.UseJwtEncryption = false;
                config.PasswordEncryptionKey = configuration["EncryptionSettings:PasswordEncryptionKey"];
                config.RedisConnectionSettings = new RedisSettings
                {
                    ConnectionMultiplexer = ConnectionMultiplexer.Connect(configuration.GetConnectionString("Redis")),
                    DatabaseNumber = 1
                };
                config.JwtSettings = new JwtSettings
                {
                    SigningKey = configuration["JwtSettings:SigningKey"]!,
                    AccessTokenLifetime = TimeSpan.Parse(configuration["JwtSettings:AccessTokenLifetime"]!),
                    RefreshTokenMonthLifetime = int.Parse(configuration["JwtSettings:RefreshTokenMonthLifetime"]!),
                    EncryptionKey = configuration["JwtSettings:EncryptionKey"]!,
                    TokenValidationParameters = tokenValidationParameters,
                };
                config.IncludeUserClaimsInJwt = true;
            })
            .AddSecurityMediatR(x => x.StoreContractPermissions = true)
#if Example
            .AddContractValidatorsFromAssembly(typeof(PingCommandErrorMessages).Assembly, includeInternalTypes: true)
#endif
            .AddTaskQueue(config =>
            {
                config.TaskIntegrityImplementationType = typeof(TaskIntegrityService);
#if InMemoryBroker
                config.UseInMemoryBroker();

#elif RabbitMqBroker
                config.UseRabbitMQ(new RabbitMqSettings
                {
                    Host = configuration["RabbitMqSettings:Host"]!,
                    Username = configuration["RabbitMqSettings:Username"]!,
                    Password = configuration["RabbitMqSettings:Password"]!,
                    VirtualHost = configuration["RabbitMqSettings:VirtualHost"]!,
                    Port = ushort.Parse(configuration["RabbitMqSettings:Port"]!),
                });
#else
                config.UseRabbitMQ(new RabbitMqSettings
                {
                    Host = configuration["RabbitMqSettings:Host"]!,
                    Username = configuration["RabbitMqSettings:Username"]!,
                    Password = configuration["RabbitMqSettings:Password"]!,
                    VirtualHost = configuration["RabbitMqSettings:VirtualHost"]!,
                    Port = ushort.Parse(configuration["RabbitMqSettings:Port"]!),
                });
#endif
                config.RedisSettings = new RedisSettings
                {
                    ConnectionMultiplexer = ConnectionMultiplexer.Connect(configuration.GetConnectionString("Redis")),
                    DatabaseNumber = 2
                };
                config.ConsumerSettings = new ConsumerSettings
                {
                    ServerName = "WebApiSkeletonApiServer"
                };
#if Example
                config.AddTasksFromAssembly(typeof(TestTask).Assembly);
#endif
            })
            .AddMinioAssemblyDependencies(config =>
            {
                config.Endpoint = configuration["Minio:Endpoint"];
                config.AccessKey = configuration["Minio:AccessKey"];
                config.SecretKey = configuration["Minio:SecretKey"];
            })
#if Example
            .AddPingExample()
#endif
            .AddPipelineBehaviors();
        services.AddAuthentication(options =>
            {
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(jwt =>
            {
                jwt.SaveToken = true;
                jwt.TokenValidationParameters = tokenValidationParameters;
            });
        return services;
    }

    public static IServiceCollection AddPipelineBehaviors(this IServiceCollection services)
    {
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(CommandLoggingBehavior<,>));
        services.AddPermissionPipelineBehaviorsForRequestsInAssemblies([
            typeof(RegistrationCommand).Assembly, // security requests
            typeof(CommandLoggingBehavior<,>).Assembly, // template requests 
            typeof(UploadFileCommand).Assembly, // file system requests
        ]);
        services.AddValidationPipelineBehaviorsFromAssemblies(
        [
            typeof(RegistrationCommand).Assembly, // security requests
#if Example
            typeof(PingCommandErrorMessages).Assembly, // template requests
#endif
            typeof(MinioSettings).Assembly, // file system requests
        ]);
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(TransactionPipelineBehavior<,>));
        services.AddTransient(typeof(IPipelineBehavior<,>), typeof(DistributedLockPipelineBehavior<,>));
        return services;
    }
}