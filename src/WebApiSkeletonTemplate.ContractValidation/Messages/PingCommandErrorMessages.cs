﻿namespace WebApiSkeletonTemplate.ContractValidation.Messages;

public static class PingCommandErrorMessages
{
    public const string EmptyGuid = "Command cannot contain empty GUID";
}