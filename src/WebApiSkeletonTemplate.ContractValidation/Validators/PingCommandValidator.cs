﻿using WebApiSkeletonTemplate.Contracts.Commands;
using WebApiSkeletonTemplate.ContractValidation.Messages;

namespace WebApiSkeletonTemplate.ContractValidation.Validators;

internal sealed class PingCommandValidator : AbstractValidator<PingCommand>
{
    public PingCommandValidator()
    {
        RuleFor(x => x.Info.Id)
            .NotEqual(Guid.Empty)
            .WithMessage(PingCommandErrorMessages.EmptyGuid);
    }
}